<?php

namespace App\Models;

use App\Models\Barang;
use App\Models\Supplier;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pembelian extends Model
{
    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected $table = 'pembelian';

    protected $fillable = [
        'no_pembelian', 'tanggal', 'id_supplier', 'id_barang', 'jumlah_barang', 'harga_barang', 'created_at', 'created_by'
    ];
}
