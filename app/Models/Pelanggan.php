<?php

namespace App\Models;

use App\Models\Penjualan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pelanggan extends Model
{
    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected $table = 'pelanggan';

    protected $fillable = [
        'kode_pelanggan', 'nama_pelanggan', 'alamat', 'no_telepon'
    ];

    /**
     * Get the user that owns the Pelanggan
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function penjualan(): BelongsTo
    {
        return $this->belongsTo(Penjualan::class, 'id_pelanggan');
    }
}
