<?php

namespace App\Models;

use App\Models\Pelanggan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\belongsToMany;

class Penjualan extends Model
{
    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected $table = 'penjualan';

    protected $fillable = [
        'no_penjualan', 'tanggal', 'id_pelanggan', 'id_barang', 'jumlah_barang', 'harga_barang', 'created_at', 'created_by'
    ];

    /**
     * Get the user associated with the Penjualan
     *
     * @return hasMany
     */
    public function pelanggan(): hasMany
    {
        return $this->hasMany(Pelanggan::class, 'id');
    }
}
