<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Supplier extends Model
{
    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    protected $table = 'supplier';

    protected $fillable = [
        'id', 'kode_supplier', 'nama_supplier', 'alamat', 'no_telepon'
    ];
}
