<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title' => 'Supplier',
            'active' => 'Supplier',
            'results' => Supplier::get(),
            'no' => 1
        ];

        return view('dashboard.supplier.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Supplier',
            'active' => 'Supplier'
        ];

        return view('dashboard.supplier.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_supplier' => 'required|string|max:150|min:6|unique:supplier,kode_supplier',
            'nama_supplier' => 'required|string|max:200|min:6',
            'alamat' => 'required|string|min:6|max:200',
            'no_telepon' => 'required|numeric'
        ]);

        $query = Supplier::create([
            'kode_supplier' => $request->kode_supplier,
            'nama_supplier' => $request->nama_supplier,
            'alamat' => $request->alamat,
            'no_telepon' => $request->no_telepon
        ]);

        if ($query) {
            return redirect()->route('supplier.index')->with("success", "Data Baru Berhasil Ditambahkan");
        } else {
            return redirect()->back()->withInput()->with("error", "Data Baru Gagal Ditambahkan)");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'title' => 'Supplier',
            'active' => 'Supplier',
            'result' => Supplier::findOrFail($id)
        ];

        return view('dashboard.supplier.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'kode_supplier' => 'required|string|max:150|min:6',
            'nama_supplier' => 'required|string|max:200|min:6',
            'alamat' => 'required|string|min:6|max:200',
            'no_telepon' => 'required|numeric'
        ]);

        $query = Supplier::findOrFail($id);

        $query->update([
            'kode_supplier' => $request->kode_supplier,
            'nama_supplier' => $request->nama_supplier,
            'alamat' => $request->alamat,
            'no_telepon' => $request->no_telepon
        ]);

        if ($query) {
            return redirect()->route('supplier.index')->with("success", "Data Baru Berhasil Diperbaharui");
        } else {
            return redirect()->back()->withInput()->with("error", "Data Baru Gagal Diperbaharui)");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = Supplier::findOrFail($id);
        $query->delete();

        if ($query) {
            return redirect()->route('supplier.index')->with("success", "Data Baru Berhasil Dihapuskan");
        } else {
            return redirect()->back()->withInput()->with("error", "Data Baru Gagal Dihapuskan)");
        }
    }
}
