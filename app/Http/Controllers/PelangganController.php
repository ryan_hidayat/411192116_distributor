<?php

namespace App\Http\Controllers;

use App\Models\Pelanggan;
use Illuminate\Http\Request;

class PelangganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title' => 'Pelanggan',
            'active' => 'Pelanggan',
            'results' => Pelanggan::get(),
            'no' => 1
        ];

        return view('dashboard.pelanggan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Pelanggan',
            'active' => 'Pelanggan'
        ];

        return view('dashboard.pelanggan.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_pelanggan' => 'required|string|max:150|min:6|unique:pelanggan,kode_pelanggan',
            'nama_pelanggan' => 'required|string|max:200|min:6',
            'alamat' => 'required|string|min:6|max:200',
            'no_telepon' => 'required|numeric'
        ]);

        $query = Pelanggan::create([
            'kode_pelanggan' => $request->kode_pelanggan,
            'nama_pelanggan' => $request->nama_pelanggan,
            'alamat' => $request->alamat,
            'no_telepon' => $request->no_telepon
        ]);

        if ($query) {
            return redirect()->route('pelanggan.index')->with("success", "Data Baru Berhasil Ditambahkan");
        } else {
            return redirect()->back()->withInput()->with("error", "Data Baru Gagal Ditambahkan)");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function show(Pelanggan $pelanggan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'title' => 'Pelanggan',
            'active' => 'Pelanggan',
            'result' => Pelanggan::findOrFail($id)
        ];

        return view('dashboard.pelanggan.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'kode_pelanggan' => 'required|string|max:150|min:6',
            'nama_pelanggan' => 'required|string|max:200|min:6',
            'alamat' => 'required|string|min:6|max:200',
            'no_telepon' => 'required|numeric'
        ]);

        $query = Pelanggan::findOrFail($id);

        $query->update([
            'kode_pelanggan' => $request->kode_pelanggan,
            'nama_pelanggan' => $request->nama_pelanggan,
            'alamat' => $request->alamat,
            'no_telepon' => $request->no_telepon
        ]);

        if ($query) {
            return redirect()->route('pelanggan.index')->with("success", "Data Baru Berhasil Diperbaharui");
        } else {
            return redirect()->back()->withInput()->with("error", "Data Baru Gagal Diperbaharui)");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = Pelanggan::findOrFail($id);
        $query->delete();

        if ($query) {
            return redirect()->route('pelanggan.index')->with("success", "Data Baru Berhasil Dihapuskan");
        } else {
            return redirect()->back()->withInput()->with("error", "Data Baru Gagal Dihapuskan)");
        }
    }
}
