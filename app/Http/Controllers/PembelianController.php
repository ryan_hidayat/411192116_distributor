<?php

namespace App\Http\Controllers;

use DateTime;
use App\Models\Barang;
use App\Models\Supplier;
use App\Models\Pembelian;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title' => 'Pembelian',
            'active' => 'Pembelian',
            'results' => DB::table('pembelian')
                ->join('supplier', 'pembelian.id_supplier', '=', 'supplier.id')
                ->join('barang', 'pembelian.id_barang', '=', 'barang.id')
                ->join('users', 'pembelian.created_by', '=', 'users.id')
                ->select('pembelian.*', 'supplier.nama_supplier', 'barang.nama_barang', 'users.name')
                ->get(),
            'no' => 1
        ];

        return view('dashboard.pembelian.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Pembelian',
            'active' => 'Pembelian',
            'suppliers' => Supplier::get(),
            'barangs' => Barang::get()
        ];

        return view('dashboard.pembelian.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'no_pembelian' => 'required|string|max:150|min:6|unique:pembelian,no_pembelian',
            'tanggal' => 'required',
            'id_supplier' => 'required',
            'id_barang' => 'required',
            'jumlah' => 'required',
            'harga' => 'required'
        ]);

        $query = Pembelian::create([
            'no_pembelian' => $request->no_pembelian,
            'tanggal' => $request->tanggal,
            'id_supplier' => $request->id_supplier,
            'id_barang' => $request->id_barang,
            'jumlah_barang' => $request->jumlah,
            'harga_barang' => $request->harga,
            'created_at' => new DateTime(),
            'created_by' => Auth::user()->id
        ]);

        if ($query) {
            return redirect()->route('pembelian.index')->with("success", "Data Baru Berhasil Ditambahkan");
        } else {
            return redirect()->back()->withInput()->with("error", "Data Baru Gagal Ditambahkan)");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pembelian  $pembelian
     * @return \Illuminate\Http\Response
     */
    public function show(Pembelian $pembelian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pembelian  $pembelian
     * @return \Illuminate\Http\Response
     */
    public function edit(Pembelian $pembelian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pembelian  $pembelian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pembelian $pembelian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pembelian  $pembelian
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pembelian $pembelian)
    {
        //
    }
}
