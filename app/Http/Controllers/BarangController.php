<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title' => 'Barang',
            'active' => 'Barang',
            'results' => Barang::get(),
            'no' => 1
        ];

        return view('dashboard.barang.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Barang',
            'active' => 'Barang'
        ];

        return view('dashboard.barang.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_barang' => 'required|string|max:150|min:6|unique:barang,kode_barang',
            'nama_barang' => 'required|string|max:200|min:6',
            'stok' => 'required|numeric',
            'harga' => 'required|numeric'
        ]);

        $query = Barang::create([
            'kode_barang' => $request->kode_barang,
            'nama_barang' => $request->nama_barang,
            'stok_barang' => $request->stok,
            'harga_barang' => $request->harga
        ]);

        if ($query) {
            return redirect()->route('barang.index')->with("success", "Data Baru Berhasil Ditambahkan");
        } else {
            return redirect()->back()->withInput()->with("error", "Data Baru Gagal Ditambahkan)");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show(Barang $barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'title' => 'Barang',
            'active' => 'Barang',
            'result' => Barang::findOrFail($id)
        ];

        return view('dashboard.barang.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'kode_barang' => 'required|string|max:150|min:6',
            'nama_barang' => 'required|string|max:200|min:6',
            'stok' => 'required|numeric',
            'harga' => 'required|numeric'
        ]);

        $query = Barang::findOrFail($id);

        $query->update([
            'kode_barang' => $request->kode_barang,
            'nama_barang' => $request->nama_barang,
            'stok_barang' => $request->stok,
            'harga_barang' => $request->harga
        ]);

        if ($query) {
            return redirect()->route('barang.index')->with("success", "Data Baru Berhasil Diperbaharui");
        } else {
            return redirect()->back()->withInput()->with("error", "Data Baru Gagal Diperbaharui)");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = Barang::findOrFail($id);
        $query->delete();

        if ($query) {
            return redirect()->route('barang.index')->with("success", "Data Baru Berhasil Dihapuskan");
        } else {
            return redirect()->back()->withInput()->with("error", "Data Baru Gagal Dihapuskan)");
        }
    }
}
