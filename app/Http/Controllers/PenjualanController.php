<?php

namespace App\Http\Controllers;

use DateTime;
use App\Models\Barang;
use App\Models\Pelanggan;
use App\Models\Penjualan;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title' => 'Penjualan',
            'active' => 'Penjualan',
            'results' => DB::table('penjualan')
                ->join('pelanggan', 'penjualan.id_pelanggan', '=', 'pelanggan.id')
                ->join('barang', 'penjualan.id_barang', '=', 'barang.id')
                ->join('users', 'penjualan.created_by', '=', 'users.id')
                ->select('penjualan.*', 'pelanggan.nama_pelanggan', 'barang.nama_barang', 'users.name')
                ->get(),
            'no' => 1
        ];

        return view('dashboard.penjualan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Penjualan',
            'active' => 'Penjualan',
            'results' => Penjualan::get(),
            'pelanggans' => Pelanggan::get(),
            'barangs' => Barang::get()
        ];

        return view('dashboard.penjualan.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'no_penjualan' => 'required|string|max:150|min:6|unique:penjualan,no_penjualan',
            'tanggal' => 'required',
            'id_pelanggan' => 'required',
            'id_barang' => 'required',
            'jumlah' => 'required',
            'harga' => 'required'
        ]);

        $cekStok = Barang::findOrFail($request->id_barang);

        if ($request->jumlah > $cekStok->stok_barang) {
            return redirect()->route('penjualan.index')->with("error", "Jumlah Stock Terlalu Sedikit");
        }

        $query = Penjualan::create([
            'no_penjualan' => $request->no_penjualan,
            'tanggal' => $request->tanggal,
            'id_pelanggan' => $request->id_pelanggan,
            'id_barang' => $request->id_barang,
            'jumlah_barang' => $request->jumlah,
            'harga_barang' => $request->harga,
            'created_at' => new DateTime(),
            'created_by' => Auth::user()->id
        ]);

        if ($query) {
            return redirect()->route('penjualan.index')->with("success", "Data Baru Berhasil Ditambahkan");
        } else {
            return redirect()->back()->withInput()->with("error", "Data Baru Gagal Ditambahkan");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function show(Penjualan $penjualan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function edit(Penjualan $penjualan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Penjualan $penjualan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penjualan $penjualan)
    {
        //
    }
}
