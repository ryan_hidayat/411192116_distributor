<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddMysqlTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Trigger Pembelian Barang
        DB::unprepared('CREATE TRIGGER `deleteStok` AFTER DELETE ON `pembelian` FOR EACH ROW UPDATE barang SET barang.stok_barang = barang.stok_barang - old.jumlah_barang WHERE barang.id = old.id_barang');
        DB::unprepared('CREATE TRIGGER `insertStok` AFTER INSERT ON `pembelian` FOR EACH ROW UPDATE barang SET barang.stok_barang = barang.stok_barang + NEW.jumlah_barang WHERE barang.id = NEW.id_barang');
        DB::unprepared('CREATE TRIGGER `updateStokKurang` AFTER UPDATE ON `pembelian` FOR EACH ROW UPDATE barang SET barang.stok_barang = barang.stok_barang - old.jumlah_barang WHERE barang.id = old.id_barang');
        DB::unprepared('CREATE TRIGGER `updateStokTambah` AFTER UPDATE ON `pembelian` FOR EACH ROW UPDATE barang SET barang.stok_barang = barang.stok_barang + NEW.jumlah_barang WHERE barang.id = NEW.id_barang');

        // Trigger Penjualan Barang
        DB::unprepared('CREATE TRIGGER `deleteStock` AFTER DELETE ON `penjualan` FOR EACH ROW UPDATE barang SET barang.stok_barang = barang.stok_barang + old.jumlah_barang WHERE barang.id = old.id_barang');
        DB::unprepared('CREATE TRIGGER `insertStock` AFTER INSERT ON `penjualan` FOR EACH ROW UPDATE barang SET barang.stok_barang = barang.stok_barang - NEW.jumlah_barang WHERE barang.id = NEW.id_barang');
        DB::unprepared('CREATE TRIGGER `updateStockKurang` AFTER UPDATE ON `penjualan` FOR EACH ROW UPDATE barang SET barang.stok_barang = barang.stok_barang + old.jumlah_barang WHERE barang.id = old.id_barang');
        DB::unprepared('CREATE TRIGGER `updateStockTambah` AFTER UPDATE ON `penjualan` FOR EACH ROW UPDATE barang SET barang.stok_barang = barang.stok_barang - NEW.jumlah_barang WHERE barang.id = NEW.id_barang');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Trigger Pembelian Barang
        DB::unprepared('DROP TRIGGER IF EXISTS `deleteStok`');
        DB::unprepared('DROP TRIGGER IF EXISTS `insertStok`');
        DB::unprepared('DROP TRIGGER IF EXISTS `updateStokKurang`');
        DB::unprepared('DROP TRIGGER IF EXISTS `updateStokTambah`');

        // Trigger Penjualan Barang
        DB::unprepared('DROP TRIGGER IF EXISTS `deleteStock`');
        DB::unprepared('DROP TRIGGER IF EXISTS `insertStock`');
        DB::unprepared('DROP TRIGGER IF EXISTS `updateStockKurang`');
        DB::unprepared('DROP TRIGGER IF EXISTS `updateStockTambah`');
    }
}
