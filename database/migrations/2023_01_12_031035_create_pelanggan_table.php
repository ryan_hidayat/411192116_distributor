<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePelangganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pelanggan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_pelanggan', 10);
            $table->string('nama_pelanggan', 200)->nullable();
            $table->text('alamat')->nullable();
            $table->string('no_telepon', 15)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Pelanggan');
    }
}
