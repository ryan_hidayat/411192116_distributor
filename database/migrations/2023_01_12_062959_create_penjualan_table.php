<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenjualanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Penjualan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_penjualan', 15);
            $table->date('tanggal');
            $table->integer('id_pelanggan');
            $table->integer('id_barang');
            $table->integer('jumlah_barang')->nullable();
            $table->integer('harga_barang')->nullable();
            $table->dateTimeTz('created_at', $precision = 0)->nullable();
            $table->char('created_by', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Penjualan');
    }
}
