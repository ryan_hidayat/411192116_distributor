@extends('layouts.app')

@section('content')
    <div class="p-5">
        <div class="card border-success">
            <div class="card-header bg-transparent d-flex justify-content-between align-items-center border-success">
                Form Pembelian
                <a href="{{ route('pembelian.create') }}" class="btn btn-outline-primary btn-sm">Tambah Data</a>
            </div>
            <div class="card-body table-responsive">
                <table id="example" class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No Pembelian</th>
                            <th>Tanggal</th>
                            <th>Supplier</th>
                            <th>Barang</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>Created At</th>
                            <th>Created By</th>
                            {{-- <th>Aksi</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($results as $res)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $res->no_pembelian }}</td>
                                <td>{{ $res->tanggal }}</td>
                                <td>{{ $res->nama_supplier }}</td>
                                <td>{{ $res->nama_barang }}</td>
                                <td>{{ $res->jumlah_barang }}</td>
                                <td>{{ $res->harga_barang }}</td>
                                <td>{{ $res->created_at }}</td>
                                <td>{{ $res->name }}</td>
                                {{-- <td>
                                    <form onsubmit="return confirm('Apakah anda yakin?')"
                                        action="{{ route('pembelian.destroy', $res->id) }}" method="POST">
                                        <a href="{{ route('pembelian.edit', $res->id) }}"
                                            class="btn btn-outline-warning btn-sm btn-block">Edit</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit"
                                            class="btn btn-outline-danger btn-sm btn-block">Hapus</button>
                                    </form>
                                </td> --}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
