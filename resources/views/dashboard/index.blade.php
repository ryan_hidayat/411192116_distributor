@extends('layouts.app')

@section('content')
    @guest()
        <div class="p-5">
            <div class="container-fluid py-5 text-center">
                <h1 class="display-5 fw-bold">Aplikasi Sistem Gudang Distributor</h1>
            </div>
        </div>
    @else
        <div class="p-5">
            <div class="container-fluid py-5 text-center">
                <h1 class="display-5 fw-bold">Selamat Datang Kembali, {{ Auth::user()->name }}</h1>
            </div>
        </div>
    @endguest
@endsection
