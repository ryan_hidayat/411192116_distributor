@extends('layouts.app')

@section('content')
    <div class="p-5">
        <div class="card border-success">
            <div class="card-header bg-transparent d-flex justify-content-between align-items-center border-success">
                Form Pelanggan
                <a href="{{ route('pelanggan.create') }}" class="btn btn-outline-primary btn-sm">Tambah Data</a>
            </div>
            <div class="card-body table-responsive">
                <table id="example" class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Kode Pelanggan</th>
                            <th>Nama Pelanggan</th>
                            <th>alamat</th>
                            <th>No Telepon</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($results as $res)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $res->kode_pelanggan }}</td>
                                <td>{{ $res->nama_pelanggan }}</td>
                                <td>{{ $res->alamat }}</td>
                                <td>{{ $res->no_telepon }}</td>
                                <td>
                                    <form onsubmit="return confirm('Apakah anda yakin?')"
                                        action="{{ route('pelanggan.destroy', $res->id) }}" method="POST">
                                        <a href="{{ route('pelanggan.edit', $res->id) }}"
                                            class="btn btn-outline-warning btn-sm btn-block">Edit</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit"
                                            class="btn btn-outline-danger btn-sm btn-block">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
