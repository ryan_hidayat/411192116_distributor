@extends('layouts.app')

@section('content')
    <div class="p-5">
        <div class="container">
            <div class="card border-success">
                <div class="card-header bg-transparent d-flex justify-content-between align-items-center border-success">
                    Edit Data Pelanggan
                    <a href="{{ route('pelanggan.index') }}" class="btn btn-outline-success btn-sm">Kembali</a>
                </div>
                <div class="card-body ">
                    <form action="{{ route('pelanggan.update', $result->id) }}" method="post">
                        @csrf
                        @method('PUT')

                        <div class="mb-3 row">
                            <label for="kode_pelanggan" class="col-sm-2 col-form-label">Kode Pelanggan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('kode_pelanggan') is-invalid @enderror"
                                    name="kode_pelanggan" id="kode_pelanggan" autocomplete="off"
                                    value="{{ $result->kode_pelanggan }}">
                                @error('kode_pelanggan')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="nama_pelanggan" class="col-sm-2 col-form-label">Nama Pelanggan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('nama_pelanggan') is-invalid @enderror"
                                    name="nama_pelanggan" id="nama_pelanggan" autocomplete="off"
                                    value="{{ $result->nama_pelanggan }}">
                                @error('nama_pelanggan')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('alamat') is-invalid @enderror"
                                    name="alamat" id="alamat" autocomplete="off" value="{{ $result->alamat }}">
                                @error('alamat')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="no_telepon" class="col-sm-2 col-form-label">No Telepon</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('no_telepon') is-invalid @enderror"
                                    name="no_telepon" id="no_telepon" autocomplete="off" value="{{ $result->no_telepon }}">
                                @error('no_telepon')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div>
                            <button class="btn btn-warning btn-sm" type="submit">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
