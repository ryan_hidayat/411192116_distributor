@extends('layouts.app')

@section('content')
    <div class="p-5">
        <div class="container">
            <div class="card border-success">
                <div class="card-header bg-transparent d-flex justify-content-between align-items-center border-success">
                    Tambah Data Penjualan
                    <a href="{{ route('penjualan.index') }}" class="btn btn-outline-success btn-sm">Kembali</a>
                </div>
                <div class="card-body ">
                    <form action="{{ route('penjualan.store') }}" method="post">
                        @csrf
                        <div class="mb-3 row">
                            <label for="no_penjualan" class="col-sm-2 col-form-label">No penjualan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('no_penjualan') is-invalid @enderror"
                                    name="no_penjualan" id="no_penjualan" autocomplete="off"
                                    value="{{ old('no_penjualan') }}">
                                @error('no_penjualan')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="tanggal" class="col-sm-2 col-form-label">Tanggal</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control @error('tanggal') is-invalid @enderror"
                                    name="tanggal" id="tanggal" autocomplete="off" value="{{ old('tanggal') }}">
                                @error('tanggal')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="pelanggan" class="col-sm-2 col-form-label">Pelanggan</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="id_pelanggan" required>
                                    <option value="">--- Pilih Pelanggan ---</option>
                                    @foreach ($pelanggans as $res)
                                        <option value="{{ $res->id }}">{{ $res->nama_pelanggan }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="barang" class="col-sm-2 col-form-label">Barang</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="id_barang" required>
                                    <option value="">--- Pilih Barang ---</option>
                                    @foreach ($barangs as $res)
                                        <option value="{{ $res->id }}">{{ $res->nama_barang }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="jumlah" class="col-sm-2 col-form-label">Jumlah</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('jumlah') is-invalid @enderror"
                                    name="jumlah" id="jumlah" autocomplete="off" value="{{ old('jumlah') }}">
                                @error('jumlah')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="harga" class="col-sm-2 col-form-label">Harga</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('harga') is-invalid @enderror"
                                    name="harga" id="harga" autocomplete="off" value="{{ old('harga') }}">
                                @error('harga')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div>
                            <button class="btn btn-primary btn-sm" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
