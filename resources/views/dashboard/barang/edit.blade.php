@extends('layouts.app')

@section('content')
    <div class="p-5">
        <div class="container">
            <div class="card border-success">
                <div class="card-header bg-transparent d-flex justify-content-between align-items-center border-success">
                    Edit Data Barang
                    <a href="{{ route('barang.index') }}" class="btn btn-outline-success btn-sm">Kembali</a>
                </div>
                <div class="card-body ">
                    <form action="{{ route('barang.update', $result->id) }}" method="post">
                        @csrf
                        @method('PUT')

                        <div class="mb-3 row">
                            <label for="kode_barang" class="col-sm-2 col-form-label">Kode Barang</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('kode_barang') is-invalid @enderror"
                                    name="kode_barang" id="kode_barang" autocomplete="off"
                                    value="{{ $result->kode_barang }}">
                                @error('kode_barang')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="nama_barang" class="col-sm-2 col-form-label">Nama Barang</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('nama_barang') is-invalid @enderror"
                                    name="nama_barang" id="nama_barang" autocomplete="off"
                                    value="{{ $result->nama_barang }}">
                                @error('nama_barang')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="stok" class="col-sm-2 col-form-label">Stok</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('stok') is-invalid @enderror"
                                    name="stok" id="stok" autocomplete="off" value="{{ $result->stok_barang }}">
                                @error('stok')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="harga" class="col-sm-2 col-form-label">Harga</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('harga') is-invalid @enderror"
                                    name="harga" id="harga" autocomplete="off" value="{{ $result->harga_barang }}">
                                @error('harga')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div>
                            <button class="btn btn-warning btn-sm" type="submit">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
