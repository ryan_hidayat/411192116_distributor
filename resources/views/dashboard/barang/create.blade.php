@extends('layouts.app')

@section('content')
    <div class="p-5">
        <div class="container">
            <div class="card border-success">
                <div class="card-header bg-transparent d-flex justify-content-between align-items-center border-success">
                    Tambah Data Barang
                    <a href="{{ route('barang.index') }}" class="btn btn-outline-success btn-sm">Kembali</a>
                </div>
                <div class="card-body ">
                    <form action="{{ route('barang.store') }}" method="post">
                        @csrf
                        <div class="mb-3 row">
                            <label for="kode_barang" class="col-sm-2 col-form-label">Kode Barang</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('kode_barang') is-invalid @enderror"
                                    name="kode_barang" id="kode_barang" autocomplete="off" value="{{ old('kode_barang') }}">
                                @error('kode_barang')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="nama_barang" class="col-sm-2 col-form-label">Nama Barang</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('nama_barang') is-invalid @enderror"
                                    name="nama_barang" id="nama_barang" autocomplete="off" value="{{ old('nama_barang') }}">
                                @error('nama_barang')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="stok" class="col-sm-2 col-form-label">Stok</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('stok') is-invalid @enderror"
                                    name="stok" id="stok" autocomplete="off" value="{{ old('stok') }}">
                                @error('stok')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="harga" class="col-sm-2 col-form-label">Harga</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('harga') is-invalid @enderror"
                                    name="harga" id="harga" autocomplete="off" value="{{ old('harga') }}">
                                @error('harga')
                                    <div class="form-text text-danger">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div>
                            <button class="btn btn-primary btn-sm" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
