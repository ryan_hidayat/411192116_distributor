<nav class="navbar navbar-expand-lg navbar-dark bg-success">
    <div class="container">
        <a class="navbar-brand" href="#">Distributor</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link {{ $active == 'Dashboard' ? 'active' : '' }}" href="{{ url('/') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ $active == 'Barang' ? 'active' : '' }}" href="{{ url('barang') }}">Barang</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ $active == 'Pelanggan' ? 'active' : '' }}"
                        href="{{ url('pelanggan') }}">Pelanggan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ $active == 'Supplier' ? 'active' : '' }}"
                        href="{{ url('supplier') }}">Supplier</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ $active == 'Pembelian' ? 'active' : '' }}"
                        href="{{ url('pembelian') }}">Pembelian</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ $active == 'Penjualan' ? 'active' : '' }}"
                        href="{{ url('penjualan') }}">Penjualan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('logout') }}">Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
