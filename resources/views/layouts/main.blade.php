<!doctype html>
<html lang="en">

<head>
    @include('partikels.head')
</head>

<body>
    @include('partikels.navbar')

    <div class="main-content">
        @yield('content')
    </div>

    @include('partikels.footer')
</body>

</html>
